module disroot.org/kitzman/chessfs

go 1.13

replace 9fans.net/go v0.0.2 => github.com/psilva261/go v0.0.0-20211226113545-7b63d477c61e

require (
	9fans.net/go v0.0.2
	github.com/knusbaum/go9p v0.24.0
	github.com/notnil/chess v1.9.0
)
