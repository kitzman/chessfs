package main

import (
	"flag"
	"log"
	"os"
	"os/user"

	"github.com/knusbaum/go9p"
	"github.com/knusbaum/go9p/fs"
)

func main() {
	srv := flag.String("srv", "chessfs", "server name")
	addr := flag.String("address", "", "serve over tcp")
	gname := flag.String("group", "", "group name, defaults to username")
	//	single := flag.Bool("single", false, "single-game filesystem")
	verbose := flag.Bool("verbose", false, "verbose")
	flag.Parse()

	if flag.NArg() > 0 {
		log.Printf("Extraneous arguments.")
		flag.Usage()
		os.Exit(1)
	}
	user, _ := user.Current()
	var group = user.Username
	if gname != nil {
		group = *gname
	}

	chessfs, _ := fs.NewFS(user.Name, user.Name, 0555)
	rootdir, ok := chessfs.Root.(fs.ModDir)
	if !ok {
		log.Fatal("root dir is not modifiable")
	}

	statctl := chessfs.NewStat("ctl", user.Username, group, 0600)
	statclone := chessfs.NewStat("clone", user.Username, group, 0440)
	statgames := chessfs.NewStat("games", user.Username, group, 0555)

	room := NewGameRoom(*verbose)
	gamedir := fs.NewStaticDir(statgames)

	rootdir.AddChild(NewCtlFile(room, statctl))
	rootdir.AddChild(NewCloneFile(room, statclone, chessfs, gamedir))
	rootdir.AddChild(gamedir)

	if *addr == "" {
		go9p.PostSrv(*srv, chessfs.Server())
	} else {
		go9p.Serve(*addr, chessfs.Server())
	}
}
